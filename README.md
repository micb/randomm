Functions and structurure to generate random numbers without repeating already generated ones.

Sample function use code:
```
#include <stdio.h>

#include "randomm.h"

int
main (void)
{
    /* Create RandMem item */
    RandMem *rm_mem = randomm_new ();

    /* Set random numbers range */
    randomm_set_range (rm_mem, 100);

    /* Generate numbers */
    for (int j = 0; j < 200; ++j) {
        printf ("Random number : %lu\n", randomm_get_number (rm_mem));
    }

    /* Free RandMem item */
    randomm_free (rm_mem);
}
```

My email in case of problems: michal.babik@protonmail.com

