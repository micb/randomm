/**
 * @file  rantest.c
 * @copyright Copyright (C) 2019-2020 Michał Bąbik
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @brief  Random without repeated values - example
 *
 * @author Michal Babik <michal.babik@protonmail.com>
 */
#include <stdio.h>
#include <stdlib.h>

#include "randomm.h"

int
main (void)
{
    /* Create RandMem item */
    RandMem *rm_mem = randomm_new ();

    /* Set random numbers range */
    randomm_set_range (rm_mem, 100);

    /* Generate numbers */
    for (int j = 0; j < 100; ++j) {
        printf ("Random number : %lu\n", randomm_get_number (rm_mem));
    }
    printf ("\n");
    for (int j = 0; j < 100; ++j) {
        printf ("Random number : %lu\n", randomm_get_number (rm_mem));
    }

    /* Free RandMem item */
    randomm_free (rm_mem);
}

